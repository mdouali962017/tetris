#ifndef PIECE_H
#define PIECE_H

class Piece{

  private:
   int forme;
   int rotation;
   int color;
  public:
  Piece::Piece();
  Piece::Piece(int type,int rot);
  int getForme() const;
  int getRotation()const;
  int getCouleur()const;




}

#endif // PIECE_H
